We are reorganizing this code so that it:

Mazacoin is directly forked from it's bitcoin source
produces proper gitian-builds
moves all significant deviations from bitcoin out of the main code so differences betwee MAZA and BTC are clear
provides a proper testnet environment
Maza Core integration/staging tree
Build Status

https://www.bitcoin.org

What is Bitcoin?
Bitcoin is an experimental new digital currency that enables instant payments to anyone, anywhere in the world. Bitcoin uses peer-to-peer technology to operate with no central authority: managing transactions and issuing money are carried out collectively by the network. Bitcoin Core is the name of open source software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of the Bitcoin Core software, see https://www.bitcoin.org/en/download.

* Content is available under GNU License : https://www.gnu.org/

Mazacoin is released under the terms of the MIT license. ===>  http://opensource.org/licenses/MIT.

